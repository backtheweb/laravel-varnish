<?php

namespace Backtheweb\Varnish;

use Illuminate\Support\ServiceProvider;
use Backtheweb\Varnish\Commands\FlushVarnishCacheCommand;

class VarnishServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $configPath = __DIR__.'/../config/varnish.php';

            $publishPath = base_path('config/varnish.php');

            $this->publishes([$configPath => $publishPath], 'config');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/varnish.php', 'varnish');

        $this->app->bind('command.varnish:flush', FlushVarnishCacheCommand::class);

        $this->app->singleton('varnish', function ($app) {

            $admin = new Admin(
                config('varnish.admin.host'),
                config('varnish.admin.port'),
                config('varnish.admin.secretFile')
            );

            return new Varnish(
                $admin,
                config('varnish.hosts'),
                config('varnish.header_name')
            );
        });

        $this->commands([
            'command.varnish:flush',
        ]);
    }

    public function provides()
    {
        return [
            'varnish',
        ];
    }
}
