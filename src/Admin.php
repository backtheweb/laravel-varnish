<?php

namespace Backtheweb\Varnish;


use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Admin
{
    public $host;
    public $port;
    public $secretFile;

    public function __construct($host = '127.0.0.1', $port = 6082, $secretFile = '/etc/varnish/secret')
    {
        $this->host       = $host;
        $this->port       = $port;
        $this->secretFile = $secretFile;
    }

    public function execute(string $command): Process
    {

        $command = "varnishadm -S {$this->secretFile} -T {$this->host}:{$this->port} '{$command}'";

        dump($command);

        $process = new Process($command);

        $process->run();

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process;
    }
}
