<?php

namespace Backtheweb\Varnish\Commands;

use Backtheweb\Varnish\Facade\Varnish;
use Illuminate\Console\Command;

class FlushVarnishCacheCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'varnish:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush the varnish cache.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Varnish::flush();

        $this->comment('The varnish cache has been flushed!:');

    }
}
