<?php

namespace Backtheweb\Varnish\Middleware;

use Closure;

class VarnishCache
{
    public function handle($request, Closure $next, int $ttl = null)
    {
        $response = $next($request);

        return $response->withHeaders([
            config('varnish.header_name', 'X-Cacheable') => '1',
            'Cache-Control' => 'public, max-age=' . ( $ttl ?? config('varnish.default_ttl') ) * 60,
        ]);
    }
}
