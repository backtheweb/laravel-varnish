<?php

namespace Backtheweb\Varnish;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Varnish
{
    protected $hosts;

    protected $header_name;

    /** @var Admin */
    protected $admin;

    public function __construct(Admin $admin, Array $hosts = [], $header_name = 'X-Cacheable'  )
    {
        $this->admin       = $admin;
        $this->hosts       = $hosts;
        $this->header_name = $header_name;
    }

    /**
     * @param string|array $host
     *
     * @return \Symfony\Component\Process\Process
     */

    public function flush(): Process
    {
        return $this->flushHosts();
    }

    public function flushHosts(): Process
    {
        return $this->admin->execute("ban {$this->_banHostsString()}");
    }

    public function flushUrl($urlRegex)
    {
        return $this->admin->execute("ban {$this->_banHostsString()} && req.url {$urlRegex}");
    }

    public function flushSlug($slug)
    {
        return $this->flushUrl(" ~ {$slug}");
    }

    private function _banHostsString()
    {
        $hosts = collect($this->hosts)->map(function (string $host) { return "(^{$host}$)"; })->implode('|');

        return "req.http.host ~ {$hosts}";
    }

}
