Laravel Varnish
---------------

Publish the service provider

~~~ bash
php artisan vendor:publish
~~~

Setup the config **config/varnish.php**


Add the middleware in tou kernel

~~~ php
// app/Http/Kernel.php
protected $routeMiddleware = [
...
   'cacheable' => \Backtheweb\Varnish\Middleware\VarnishCache::class,
];
~~~

Setup your routes

~~~ php
// routes/web.php

Route::group(['middleware' => 'varnish'], function() {
    Route::get('/', 'HomeController@index');
});
~~~

Varnish will cache the responses of the routes inside the group for 60 minutes

~~~ php
Route::group(['middleware' => 'varnish:60'], function() {
   ...
});
~~~


Edit the *vcl_backend_response* function on yout varnish config (/etc/varnish/default.vcl)  

~~~ 
sub vcl_backend_response {

    if (beresp.http.X-Cacheable ~ "1") {
        unset beresp.http.set-cookie;
    }
}

~~~
