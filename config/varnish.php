<?php

return [

    'hosts' => [
        'yourdomian.com'
    ],

    'header_name'   => 'X-Cacheable',
    'default_ttl'   => 60,
    'admin'         => [
        'host'          => '127.0.0.1',
        'port'          => 6082,
        'secretFile'    => '/etc/varnish/secret',
    ],
];
